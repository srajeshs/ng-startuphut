var app = angular.module('app',['ui.router']);
app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
    $stateProvider
    .state('home', {
        url:'/home',
        views:
        {
            '':{
                templateUrl: 'pages/home.html',
            },
            'banner':{
                templateUrl:'pages/banner.html'
            },
        }
    })
    .state('prototype', {
        url:'/prototype',
        templateUrl: 'pages/prototype.html',
    })
    .state('development', {
        url:'/development',
        templateUrl: 'pages/development.html',
    })
    .state('why-us', {
        url:'/why-us',
        templateUrl: 'pages/why-us.html',
    })
    .state('contact', {
        url:'/contact',
        templateUrl: 'pages/contact.html',
    })
});